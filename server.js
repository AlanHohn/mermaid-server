const express = require('express');
const bodyParser = require('body-parser');
const render = require('./render');

const app = express();
const port = process.env.PORT || 3000;

const gantt = `
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
`;

app.use(bodyParser.text())

app.post('/render', function(req, res){
  render(req.body, function(content) {
    res.send(content);
  });
});

app.get('/test', function(req, res){
  render('graph TB;A-->B', function(content) {
    res.send(content);
  });
});

app.listen(port, () => console.log(`Mermaid Server on port ${port}`));
