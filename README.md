# Mermaid Server

This is a very basic Node.js / Express app that renders [Mermaid][] diagrams
server side and provides a PNG. It uses Puppeteer.

This app exists to illustrate the use of Puppeteer on Google App Engine
given the need to install some additional dependencies to get a headless
Chromium to work.

[mermaid]:https://mermaidjs.github.io/
